const url = 'https://api.openbrewerydb.org/breweries?per_page=10';
const HTMlResponse = document.querySelector("data");

breweries = [];


fetch(url)
    .then(response => response.json())
    .then(data => {
        breweries = data;

        const product = document.getElementById('row');
        const fragment = document.createDocumentFragment();

        for (let item of breweries) {

            let card = document.createElement('div');
            card.className = 'div class="col mb-4   col-md-6  col-lg-4';


            let cardDiv = document.createElement('div');
            cardDiv.className = 'card text-center';
            card.appendChild(cardDiv);


            let cardbody = document.createElement('div');
            cardbody.className = 'card-body';
            cardDiv.appendChild(cardbody);


            let title = document.createElement('h5');
            title.className = 'card-title';
            title.innerText = 'Brewery';
            cardbody.appendChild(title);


            let text = document.createElement('p');
            text.className = 'card-text';
            text.innerText = item.name;
            cardbody.appendChild(text);


            let button = document.createElement('a');
            button.className = 'btn btn-secondary btn-sm';
            button.setAttribute('type', 'button');

            button.setAttribute('data-bs-toggle', 'modal');
            button.setAttribute('data-bs-target', '#staticBackdrop');
            button.innerText = 'See Brewery';
            button.addEventListener('click', function() {

                const urlBre = 'https://api.openbrewerydb.org/breweries/' + item.id
                fetch(urlBre)
                    .then(response => response.json())
                    .then(brewery => {
                        console.log(brewery)
                        document.getElementById('name').innerHTML = 'Name: ' + brewery.name;
                        document.getElementById('city').innerHTML = 'City: ' + brewery.city;
                        document.getElementById('country').innerHTML = 'Country: ' + brewery.country;
                        document.getElementById('type').innerHTML = 'Type: ' + brewery.brewery_type;
                        document.getElementById('latitude').innerHTML = brewery.latitude ? 'Latitude: ' + brewery.latitude : '';
                        document.getElementById('longitude').innerHTML = brewery.longitude ? 'Latitude: ' + brewery.longitude : '';
                        document.getElementById('website_url').style.display = brewery.website_url ? 'block' : 'none';
                        document.getElementById('website_url').innerHTML = 'Web site';
                        document.getElementById('website_url').setAttribute('href', brewery.website_url);

                    });

            });

            cardbody.appendChild(button);


            fragment.appendChild(card);

        }
        product.appendChild(fragment);

    });