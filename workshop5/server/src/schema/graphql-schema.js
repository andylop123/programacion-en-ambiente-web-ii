import { gql } from "apollo-server-express";
import {makeExecutableSchema} from 'graphql-tools'

 const typeDefs =gql `
  type Query {
    post(_id: String): Post
    posts: [Post]
    comment(_id: String): Comment
    hello:String
  }
  type Mutation {
    createPost(title: String, content: String): Post
    createComment(postId: String, content: String): Comment
  }

  type Post {
    _id: String
    title: String
    content: String
    comments: [Comment]
  }
  type Comment {
    _id: String
    postId: String
    content: String
    post: Post
  } 
  `;


 
  module.exports ={typeDefs}