// const Posts = require('../models/postModel');
// const Comments = require('../models/comment');




const resolvers = {
    Query: {
        hello: () => "Hello World",
        post: async (root, { _id }) => {
            return prepare(await Posts.findOne(ObjectId(_id)))
        },
        posts: async () => {
            return (await Posts.find({}).toArray()).map(prepare)
        },
        comment: async (root, { _id }) => {
            return prepare(await Comments.findOne(ObjectId(_id)))
        },
    },
    Mutation: {
        createPost: async (post) => {
            id = post._id;
            title = post.title;

            return post;
        },
        createComment: async (root, args) => {
            const res = await Comments.insert(args)
            return prepare(await Comments.findOne({ _id: res.insertedIds[1] }))
        },
    },
}



module.exports = { resolvers };