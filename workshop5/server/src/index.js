import { ApolloServer, gql } from "apollo-server-express";
import express from 'express';


import { typeDefs } from "./schema/graphql-schema"
import { resolvers } from "./schema/resolvers";


// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");
// check for cors


async function startApolloServer() {

    const server = new ApolloServer({ typeDefs, resolvers });
    await server.start();

    const app = express();
    server.applyMiddleware({ app });
    // server.start()

    const cors = require("cors");
    app.use(cors({
        domains: '*',
        methods: "*"
    }));



    await new Promise(resolve => app.listen({ port: 3000 }, resolve));
    console.log(`🚀 Server ready at http://localhost:3000${server.graphqlPath}`);
}

startApolloServer();



