const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const post = require('../models/postModel');


const comment = new Schema({

    _id: {
        type: String
    },
    postId: { type: Schema.ObjectId, ref: post }
    ,
    content: {
        type: String
    },
    post:post.Schema
    
});




module.exports = mongoose.model('comment', comment);