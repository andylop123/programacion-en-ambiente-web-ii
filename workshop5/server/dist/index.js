"use strict";

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])(); // check for cors

var cors = require("cors");

app.use(cors({
  domains: '*',
  methods: "*"
}));
console.log();
app.listen(3000, function () {
  return console.log("Example app listening on port 3000!");
});
//# sourceMappingURL=index.js.map