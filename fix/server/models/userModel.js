const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//user model
const user = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
});

module.exports = mongoose.model('user', user);