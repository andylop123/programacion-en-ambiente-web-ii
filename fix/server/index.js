const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");
const course = require("./controller/userController");

// const {
//     coursePost,
//     courseGet,
//     courseDelete,
//     coursePatch
// } = require("./controller/userController");
// const {
//     userPost
// } = require("./controller/courseController.js");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));





// listen to the task request
app.get("/course", course.courseGet);
// app.post("/course", course.coursePost);
// app.patch("/course", coursePatch);
// app.delete("/course", courseDelete);

// listen to the user request
// app.post("/user", userPost);

app.listen(3000, () => console.log(`Example app listening on port 3000!`))