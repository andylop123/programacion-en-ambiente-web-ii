const express = require('express');
const cors = require("cors");
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");


const Task = require("./taskModel");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());




app.post('/test', function(req, res) {


    const task = new Task();
    task.detail = req.body.detail;

    res.send(`{
        "body" : "${task.detail}",
        "TipoVentaDolares" : "621",
        "TipoCompraEuros" : "731.85",
        "TipoVentaEuros" : "761.9"
      }`);

});




app.post('/insert', function(req, res) {
    // Create a task
    const task = new Task();

    task.title = req.body.title;
    task.detail = req.body.detail;

    console.log(req.body);

    // // Validate request
    if (!req.body.title || !req.body.detail) {
        return res.status(422).send({
            message: "Note content can not be empty"
        });
    }


    // Save Note in the database
    if (task) {
        task.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the task."
                });
            });
    }

});







app.get('/all', function(req, res) {

    Task.find()
        .then(tasks => {
            res.send(tasks);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });

});

app.put('/edit', function(req, res) {


    var task = new Task();
    task.id = req.body.id;

    if (req.query && req.query.id) {
        Task.findById(req.query.id, function(err, task) {
            if (err) {
                res.status(404);
                console.log('error while queryting the task', err)
                res.json({ error: "Task doesnt exist" })
            }

            // update the task object (patch)
            task.title = req.body.title ? req.body.title : task.title;
            task.detail = req.body.detail ? req.body.detail : task.detail;
            // update the task object (put)
            // task.title = req.body.title
            // task.detail = req.body.detail

            task.save(function(err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the task', err)
                    res.json({
                        error: 'There was an error saving the task'
                    });
                }
                res.status(200); // OK
                res.json(task);
            });
        });
    } else {
        res.status(404);
        res.json({ error: "Task doesnt exist" })
    }

});






app.listen(3000, () => console.log(`Example app listening on port 3000!`))