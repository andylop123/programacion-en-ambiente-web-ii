const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");


const {
    coursePost,
    courseGet,
    courseDelete,
    coursePatch
} = require("./controller/courseController.js");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));


// listen to the task request
app.get("/api/course", courseGet);
app.post("/api/course", coursePost);
app.patch("/api/course", coursePatch);
app.delete("/api/course", courseDelete);


app.listen(3000, () => console.log(`Example app listening on port 3000!`))