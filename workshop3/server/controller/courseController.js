const Course = require("../models/coursModel");


/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const coursePost = (req, res) => {
    var course = new Course();

    course.name = req.body.name;
    course.code = req.body.code;
    course.career = req.body.career;
    course.credits = req.body.credits;

    if (course) {
        course.save(function(err) {
            if (err) {
                res.status(422);
                console.log('error while saving the course', err)
                res.json({
                    error: 'There was an error saving the course'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/course/?id=${course.id}`
            });
            res.json(course);
        });
    } else {
        res.status(422);
        console.log('error while saving the course')
        res.json({
            error: 'No valid data provided for course'
        });
    }
};


const courseGet = (req, res) => {
    // if an specific course is required
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function(err, course) {
            if (err) {
                res.status(404);
                console.log('error while queryting the Course', err)
                res.json({ error: "Course doesnt exist" })
            }
            res.json(course);
        });
    } else {
        // get all courses
        Course.find(function(err, courses) {
            if (err) {
                res.status(422);
                res.json({ "error": err });
            }
            res.json(courses);
        });

    }

};


/**
 * Delete one course
 *
 * @param {*} req
 * @param {*} res
 */
const courseDelete = (req, res) => {
    // if an specific task is required
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function(err, course) {
            if (err) {
                res.status(500);
                console.log('error while queryting the course', err)
                res.json({ error: "course doesnt exist" })
            }
            //if the task exists
            if (course) {
                course.remove(function(err) {
                    if (err) {
                        res.status(500).json({ message: "There was an error deleting the course" });
                    }
                    res.status(204).json({});
                })
            } else {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({ error: "course doesnt exist" })
            }
        });
    } else {
        res.status(404).json({ error: "You must provide a course ID" });
    }
};

/**
 * Update a course
 *
 * @param {*} req
 * @param {*} res
 */
const coursePatch = (req, res) => {
    // get course by id
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function(err, course) {
            if (err) {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({ error: "Course doesnt exist" })
            }

            // update the course object (patch)

            course.name = req.body.name ? req.body.name : course.name;
            course.code = req.body.code ? req.body.code : course.code;
            course.career = req.body.career ? req.body.career : course.career;
            course.credits = req.body.credits ? req.body.credits : course.credits;

            course.save(function(err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the course', err)
                    res.json({
                        error: 'There was an error saving the course'
                    });
                }
                res.status(200); // OK
                res.json(course);
            });
        });
    } else {
        res.status(404);
        res.json({ error: "Course doesnt exist" })
    }
};


module.exports = { coursePost, courseGet, courseDelete, coursePatch };