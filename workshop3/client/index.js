const url = 'http://localhost:3000/api/course';


const error = (e) => console.log(e.target.responseText);

function saveCourse() {
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("POST", "http://localhost:3000/api/course");
    ajaxRequest.setRequestHeader("Content-Type", "application/json");

    const course = {
        'name': document.getElementById('name').value,
        'code': document.getElementById('code').value,
        'career': document.getElementById('career').value,
        'credits': document.getElementById('credits').value
    };
    ajaxRequest.send(JSON.stringify(course));
    deleteInputs();
    let row = document.getElementById("row");
    row.innerHTML = '';
    data();


}


function editCourse() {
    let id = document.getElementById('ID').value;
    const ajaxRequest = new XMLHttpRequest();
    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("PATCH", "http://localhost:3000/api/course?id=" + id);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");

    const course = {
        'name': document.getElementById('eName').value,
        'code': document.getElementById('eCode').value,
        'career': document.getElementById('eCareer').value,
        'credits': document.getElementById('eCredits').value
    };
    ajaxRequest.send(JSON.stringify(course));
    data();


}



function deleteCourse(id) {
    newUrl = `http://localhost:3000/api/course?id=${id}`;
    const ajaxRequest = new XMLHttpRequest();

    ajaxRequest.addEventListener("error", error);
    ajaxRequest.open("DELETE", newUrl);
    ajaxRequest.setRequestHeader("Content-Type", "application/json");
    ajaxRequest.send();

}




function data() {
    let row = document.getElementById("row");
    const fragment = document.createDocumentFragment();
    row.innerHTML = '';
    fetch(url)
        .then(response => response.json())
        .then(data => {
            courses = data;
            console.log(data);
            for (let item of data) {
                let id = item._id;
                row.innerHTML += `<tr>
                <th scope="row">${item.name}</th>
                <td>${item.code}</td>
                <td>${item.career}</td>
                <td>${item.credits}</td>
                <td>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"  onclick="get('${item._id}')">
                Edit
                </button>
                <button type="button" class="btn btn-primary" onclick="deleteCourse('${item._id}')">
                Delete
                </button>
                </td>
                </tr>`;
                // fragment.appendChild(tr);
            }

            // row.appendChild(fragment);


        });

}

function get(id) {
    newUrl = `http://localhost:3000/api/course?id=${id}`;
    console.log(newUrl);
    fetch(newUrl)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            document.getElementById("ID").value = data._id;
            document.getElementById("eName").value = data.name;
            document.getElementById("eCode").value = data.code;
            document.getElementById("eCareer").value = data.career;
            document.getElementById("eCredits").value = data.credits;
        })
}






function deleteInputs() {
    let inputs = document.querySelectorAll('form input ');
    for (let item of inputs) {
        item.value = '';
    }
}


$(document).ready(function() {
    data();

    $("#save").click(function(evt) {
        evt.preventDefault();
        saveCourse();
    })


    $("#sc").click(function(evt) {
        evt.preventDefault();
        editCourse();
    })


});