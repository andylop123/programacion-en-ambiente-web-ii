const express = require('express');
const router = express.Router();


const course = require('../controllers/courseController')
const user = require('../controllers/userController')




router.get("/user", user.userLogin);

router.use(function (req, res, next) {


    const bearerHeader = req.headers['authorization'];

    if (bearerHeader) {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    } else {
        res.status(401);
        res.send({
            error: "Unauthorized "
        });
    }
});







router.post("/user", (req, res) => { return user.UserPost(req, res) });



router.get("/course", course.courseGet);
router.post("/course", (req, res) => { return course.coursePost(req, res) });


module.exports = router;