const userModel = require("../models/userModel");
const bcrypt = require('bcrypt');

const BCRYPT_SALT_ROUND = 10;

class User {
    async UserPost(req, res) {
        var user = new userModel(req.body);
        bcrypt.hash(user.password, BCRYPT_SALT_ROUND)
            .then(function (hashedPassword) {
                user.password = hashedPassword;
                if (user) {
                    user.save(function (err) {
                        if (err) {
                            res.status(422);
                            console.log('error while saving the user', err)
                            res.json({
                                error: 'There was an error saving the user'
                            });
                        }
                        res.status(201); //CREATED
                        res.header({
                            'location': `http://localhost:3000/user/?id=${user.id}`
                        });
                        res.json(user);
                    });
                } else {
                    res.status(422);
                    console.log('error while saving the user')
                    res.json({
                        error: 'No valid data provided for user'
                    });
                }

            })

    }


    async userLogin(req, res) {
        if (req.query.email && req.query.password) {

            await userModel.findOne({ "email": req.query.email }, function (err, user) {

                if (err) {
                    res.status(404);
                    res.json({ "error": "user no found" })
                }
                if (user !== null) {
                    bcrypt.compare(req.query.password, user.password, (err, respond) => {
                        if (respond) {
                            res.json(user)
                        }
                    })
                }

            })


        } else {
            res.status(500);
            res.json({ "message": "no entro" });
        }
    }
}

module.exports = new User();



