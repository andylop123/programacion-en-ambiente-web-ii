const courseModel = require("../models/courseModel");
class Course {
    async coursePost(req, res) {
        var course = new courseModel();

        course.name = req.body.name;
        course.code = req.body.code;
        course.career = req.body.career;
        course.credits = req.body.credits;

        if (course) {
            await course.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the course', err)
                    res.json({
                        error: 'There was an error saving the course'
                    });
                }
                res.status(201); //CREATED
                res.header({
                    'location': `http://localhost:3000/course/?id=${course.id}`
                });
                res.json(course);
            });
        } else {
            res.status(422);
            console.log('error while saving the course')
            res.json({
                error: 'No valid data provided for course'
            });
        }
    }

    async courseGet(req, res) {
        // if an specific course is required
        if (req.query && req.query.id) {
            await courseModel.findById(req.query.id, function (err, course) {
                if (err) {
                    res.status(404);
                    console.log('error while queryting the Course', err)
                    res.json({ error: "Course doesnt exist" })
                }
                res.json(course);
            });
        } else {
            // get all courses
            await courseModel.find(function (err, courses) {
                if (err) {
                    res.status(422);
                    res.json({ "error": err });
                }
                res.json(courses);
            });

        }
    }

}


module.exports = new Course();