const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");

//routes 
const indexRoutes = require('./routes/index');




// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());


const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));


//routes
app.use('', indexRoutes)



app.listen(3000, () => console.log(`Example app listening on port 3000!`))