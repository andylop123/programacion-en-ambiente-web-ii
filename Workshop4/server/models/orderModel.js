const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const product = require('../models/productModel');
const client = require('../models/clientModel');


const order = new Schema({
    _id: {
        type: String
    },
    client: client.schema,
    product: product.schema
});

module.exports = mongoose.model('order', order);