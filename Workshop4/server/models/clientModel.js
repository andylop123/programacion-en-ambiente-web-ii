const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const client = new Schema({

    _id: {
        type: String
    },
    lastName: {
        type: String
    },
    name: {
        type: String
    },
    email: {
        type: String
    },
});

module.exports = mongoose.model('client', client);