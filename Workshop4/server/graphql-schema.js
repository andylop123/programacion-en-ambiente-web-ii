const { buildSchema } = require('graphql');
exports.graphQLschema = buildSchema(`
  type Query {
    orders: [Order]
    clients: [Client]
    products: [Product]
    getOrder(orderId: Int!): Order
    hello: String
  }
  type Mutation {
    addClient(name: String!, lastName: String!, email: String ): Client,
    addProduct(quantity: Int!, name: String!, price: Float! ): Product,
    addOrder(clientId: String!, productId: String!): Order
  }
  type Order {
    client: Client!
    product: Product!
  }
  type Client {
    _id: ID!
    name: String!
    lastName: String!
    email: String
    website: String
  }



  type Product {
    _id: ID!
    quantity: Int!
    name: String!
    price: Float!
  }
  
  `);