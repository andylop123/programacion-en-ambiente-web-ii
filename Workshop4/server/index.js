const express = require('express');
const graphqlHTTP = require('express-graphql').graphqlHTTP;
const { graphQLschema } = require('./graphql-schema.js')
const { v4: uuidv4 } = require('uuid');
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/workshop");
const Product = require("./models/productModel");
const Client = require("./models/clientModel");
const Order = require("./models/orderModel");

const app = express();
// check for cors
const cors = require("cors");
app.use(cors({
    domains: '*',
    methods: "*"
}));





const addClient = (client) => {
    // generate the uid
    const id = uuidv4();
    // create the client object
    const newClient = new Client();
    newClient._id = id;
    newClient.name = client.name;
    newClient.lastName = client.lastName;
    newClient.email = client.email;

    // // add client to the db
    newClient.save()
        // // es6-way
        // // clients[id] = { ...client, id };
    console.log(newClient);
    return newClient;

}

const addOrder = async(req) => {
    const client = await Client.findById({ _id: req.clientId }).exec();
    const product = await Product.findById({ _id: req.productId }).exec();
    // generate the uid
    const id = uuidv4();
    // create the client object
    const newOrder = new Order();
    newOrder._id = id;
    newOrder.client = client;
    newOrder.product = product;
    await newOrder.save(newOrder);
    console.log(client);
    return null;

}

const addProduct = async(product) => {
    // generate the uid
    const id = uuidv4();
    // create the client object
    const newProduct = new Product();

    newProduct._id = id;
    newProduct.quantity = product.quantity;
    newProduct.name = product.name;
    newProduct.price = product.price;

    // // add client to the clients array

    await newProduct.save();

    // // es6-way
    // // clients[id] = { ...client, id };
    console.log(newProduct);
    return newProduct;

}


const clients = async() => {

    return await Client.find();

}
const products = async() => {

    return await Product.find();

}

// expose in the root element the different entry points of the
// graphQL service
const root = {
    hello: () => 'Hello world from GraphQL!',
    addClient: (req) => addClient(req),
    addProduct: (req) => addProduct(req),
    clients: () => clients(),
    products: () => products(),
    addOrder: (req) => addOrder(req)


};



//one single endpoint different than REST
app.use('/graphql', graphqlHTTP({
    schema: graphQLschema,
    graphiql: true,
    rootValue: root
}));
console.log();
app.listen(3000, () => console.log(`Example app listening on port 3000!`))